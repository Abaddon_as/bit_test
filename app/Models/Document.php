<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'title',
        'format',
        'hash',
        'path',
        'documentable_id',
        'documentable_type',
    ];

    public function documentable()
    {
        return $this->morphTo();
    }
}
