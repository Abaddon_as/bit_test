<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Lot extends Model
{
    protected $fillable = [
        'tender_id',
        'title',
        'amount',
    ];

    public function tender(): BelongsTo
    {
        return $this->belongsTo(Tender::class);
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(
            Document::class,
            'documentable'
        );
    }
}
