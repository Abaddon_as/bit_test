<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Tender extends Model
{
    protected $fillable = [
        'title',
        'description',
        'tender_period_start',
    ];

    public function lots(): HasMany
    {
        return $this->hasMany(Lot::class);
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(
            Document::class,
            'documentable'
        );
    }
}
